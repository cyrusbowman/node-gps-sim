var geolib = require('geolib');
var _ = require('lodash');
var moment = require('moment');

const inch = 0.0254;
var currentPosition = {latitude: 41.6324232, longitude: -87.623443};
var currentBearing = 0;

var moveInterval = null;
function startSim(interval, startPosition) {
  if (startPosition != null) {
    currentPosition = startPosition;
  }
  if (moveInterval == null) {
    moveInterval = setInterval(function() {
      move();
    }, interval || 250);
  }
}
function stopSim() {
  if (moveInterval != null) {
    stopInterval(moveInterval);
    moveInterval = null;
  }
}
function move(distInches, bearing) {
  bearing = bearing || currentBearing;
  distInches = distInches || 12;
  currentPosition = geolib.computeDestinationPoint(currentPosition, distInches, bearing);
  for (var i=0; i<gpggaListeners.length; i++) {
    gpggaListeners[i](gpsToGGA(currentPosition));
  }
}
function gpsToGGA(latlng, time) {
  var strLat = dec2str(latlng.latitude);
  var strLng = dec2str(latlng.longitude);
  time = time || moment().utc();
  var gpgga = '$GPGGA,'+time.format('HHmmss.SS')+',';
  var latDec = strLat[2] / 60.0;
  if (latDec == 1) latDec = '0.99999999999999999999'
  if (latDec == 0) latDec = '0.0';
  latDec = latDec + '';
  gpgga += (strLat[0]+'') + (strLat[1]+'') + '.' + latDec.substring(2) + ','+(latlng.latitude > 0 ? 'N' : 'S')+',';
  var lngPart1 = (strLng[0]+'') + (strLng[1]+'');
  while (lngPart1.length < 5) {
    lngPart1 = '0' + lngPart1;
  }
  var lngDec = strLng[2] / 60.0;
  if (lngDec == 1) lngDec = '0.99999999999999999999'
  if (lngDec == 0) lngDec = '0.0';
  lngDec = lngDec + '';
  gpgga += lngPart1 + '.' + lngDec.substring(2) + ','+(latlng.longitude > 0 ? 'E' : 'W')+',4,17,0.19,00400,M,47.950,M,1.2,0000*';
  gpgga += ggaChecksum(gpgga);
  return gpgga;
}
function dec2str(latorlng) {
  latorlng = parseFloat(latorlng);
  if (latorlng < 0) latorlng = latorlng * -1;
  var deg = parseInt(latorlng, 10);
  var min = parseInt((latorlng - deg) * 60, 10);
  var seconds = (latorlng - deg - (min/60.0)) * 3600;
  return [deg, min, seconds];
}
function ggaChecksum(line) {
  var charater = "";
  var checksum = 0;
  var done = false;
  if (line.indexOf('*') == -1) throw new Error('GGA needs * in order to calculate checksum');
  while (line.length > 0 && done == false) {
    charater = line.substr(0,1);
    switch(charater) {
      case "$":
      //Ignore dollar sign
      break;
      case "*":
      //end
      done = true;
      break;
      default:
      if (checksum==0) {
        checksum = charater.charCodeAt(0);
      } else {
        checksum = checksum ^ charater.charCodeAt(0);
      }
      break;
    }
    line = line.substr(1);
  }
  return _.padStart(checksum.toString(16).toUpperCase(), 2, '0');
}

var gpggaListeners = [];
function addGPGGAListener(listener) {
  gpggaListeners.push(listener);
}
function removeGPGGAListener(listener) {
  _.remove(gpggaListeners, listener);
}
function getPosition() {
  return currentPosition;
}
function getGPGGA(time) {
    return gpsToGGA(currentPosition, time);
}
function setBearing(bearing) {
  currentBearing = bearing;
}

module.exports = {
  startSim: startSim,
  stopSim: stopSim,
  gpsToGGA: gpsToGGA,
  getPosition: getPosition,
  getGPGGA: getGPGGA,
  setBearing: setBearing,
  move: move,
  addGPGGAListener: addGPGGAListener,
  removeGPGGAListener: removeGPGGAListener
};
