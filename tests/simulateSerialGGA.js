var SerialPort = require("serialport");
var Promise = require('bluebird');
var gpsSim = require('../');
var serial = null;
var recvBuffer = '';
var bytesReceived = 0;
function openSerial(baud) {
	return new Promise(function(resolve){
		serial = new SerialPort('/dev/tty.usbserial', {baudrate: parseInt(baud)});
		recvBuffer = '';
		serial.on("open", function() {
		    serial.on('error', function(err) {
		        console.log('Error: ', err);
		        throw err;
		    });
		    serial.on('data', function(buffer) {
					//console.log('Recevied', buffer.toString().length, 'bytes from serial.');
					recvBuffer = recvBuffer + buffer.toString();
					bytesReceived = bytesReceived + buffer.length;
					//process.stdout.write(buffer.toString());
				});
			resolve();
		});
	});
}
function closeSerial() {
	if (serial == null) return Promise.resolve();
	return new Promise(function(resolve){
		recvBuffer = '';
		serial.close(function() {
			resolve();
		});
	});
}
function sendData(data) {
    return new Promise(function(resolve){
        var buffer = new Buffer(data, "ascii");
        serial.write(buffer, function(err, bytesWritten) {
            if (err) console.log(err);
            if(bytesWritten < data.length) {
                console.log('Warning: Did not write all bytes... ' + bytesWritten + '/' + data.length + ' written.');
            }
            resolve(bytesWritten);
        });
    });
}
function onGGA(GGA) {
  //console.log('GGA', GGA);
  sendData(GGA+'\r\n\r\n').then(function(written) {
    //console.log(written, 'bytes written.');
  });
}

setInterval(function () {
	console.log('Speed:', Math.round(bytesReceived/5.0) + '', 'bytes/sec');
	bytesReceived = 0;
}, 5000);


//gpsSim.addGPGGAListener(onGGA);
return closeSerial().then(function() {
	return openSerial('38400');
}).then(function() {
  gpsSim.startSim(1000);
}).catch(function(err) {
	//Not this baud
  console.log('Error', err);
});
