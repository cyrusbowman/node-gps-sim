var gpsSim = require('../');
var moment = require('moment');

/*
  Move 1 min, stop for 5 seconds, move another min.
  10 hz gga
*/

var hertz = 10;
var time = moment.utc("2018-03-22T21:04:23.000Z");
console.log(gpsSim.getPosition());
//Move for 60 seconds
for (var i=0; i<60*hertz; i++) {
  console.log(gpsSim.getGPGGA(time));
  gpsSim.move(((5 * 0.3048) / hertz), 0); //5 feet a second
  time.add((1/hertz), 'seconds');
}

console.log('Stopping');
//Stop for 5 seconds
for (var i=0; i<5*hertz; i++) {
  time.add((1/hertz), 'seconds');
  console.log(gpsSim.getGPGGA(time));
}

//Move again for 60 seconds
for (var i=0; i<60*hertz; i++) {
  gpsSim.move(((5 * 0.3048) / hertz), 0); //5 feet a second
  time.add((1/hertz), 'seconds');
  console.log(gpsSim.getGPGGA(time));
}
console.log(gpsSim.getPosition())
